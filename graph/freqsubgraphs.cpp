//
// Created by squishy on 11/12/16.
//

#include "freqsubgraphs.h"
#include <QtDebug>

namespace vcfsm {
namespace graph {

FreqSubGraphs::FreqSubGraphs()
{
    NautyGraph noEdges;
    qDebug() << noEdges.hash();
    std::vector<NautyGraph> oneEdges = noEdges.augment();
    qDebug() << "oneEdges size:" << oneEdges.size();
    for (NautyGraph g : oneEdges)
    {
        qDebug() << g.hash();
        if (canonicalSubgraphs.find(g.canonical()) == canonicalSubgraphs.end())
            canonicalSubgraphs.insert({g.canonical(), std::unordered_set<NautyGraph>()});
        canonicalSubgraphs.at(g.canonical()).insert(g);
    }
    qDebug() << "Canonical Subgraphs size:" << canonicalSubgraphs.size();
}

void FreqSubGraphs::augmentSubgraphs()
{
    gBucketer oldGraphs = canonicalSubgraphs;
    canonicalSubgraphs.clear();
    for (auto csg: oldGraphs)
    {
        for (NautyGraph g: csg.second)
            augmentAndAdd(g);
    }
}

std::vector<NautyGraph> FreqSubGraphs::FSGs(const int threshold) const
{
    std::vector <NautyGraph> results;
    for (auto csg: canonicalSubgraphs)
    {
        if (csg.second.size() >= threshold)
            results.insert(results.end(), csg.second.begin(), csg.second.end());
            //results.push_back(csg.first);
    }
    return results;
}

std::vector<NautyGraph> FreqSubGraphs::FSGs(const std::vector<constraint_t> constraints, const int threshold) const
{
    std::vector <NautyGraph> results;
    for (auto csg: canonicalSubgraphs)
    {
        std::vector<NautyGraph> valid;
        if (csg.second.size() >= threshold)
            for (auto graph: csg.second)
                for (auto constraint: constraints)
                    if (constraint(graph))
                        valid.push_back(graph);
        if (valid.size() >= threshold)
            results.insert(results.end(), valid.begin(), valid.end());
            //results.push_back(csg.first);
    }
    return results;
}

void FreqSubGraphs::augmentAndAdd(const NautyGraph & g)
{
    std::vector<NautyGraph> augmented = g.augment();
    for (NautyGraph g: augmented)
    {
        canonicalSubgraphs[g.canonical()].insert(g);
    }
}

}
}
