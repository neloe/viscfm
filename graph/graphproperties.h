//
// Created by squishy on 10/13/2016.
//

#ifndef VCFSM_GRAPHPROPERTIES_H
#define VCFSM_GRAPHPROPERTIES_H

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <graphics/node.h>
#include <graphics/edge.h>
namespace vcfsm {
namespace graph {

class GraphProperties
{
    private:
        static std::unordered_map<const graphics::Node*, std::string> m_names;
        static std::unordered_map<const graphics::Node*, std::string> m_labels;

        static std::string defaultGet(std::unordered_map<const graphics ::Node*, std::string> &map, const graphics::Node * key);

    public:
        static void setName(const graphics::Node * node, std::string name) {m_names[node] = name;}
        static void setLabel(const graphics::Node * node, std::string label) {m_labels[node] = label;}

        static void stash();
        static void unstash();

        static std::string name(const graphics::Node * node);
        static std::string label(const graphics::Node * node);
        static std::unordered_set<graphics::Edge *> edgeSet, edgeSetStash;
        static std::unordered_set<graphics::Node *> nodeSet, nodeSetStash;

};

}
}

#endif //VCFSM_GRAPHPROPERTIES_H
