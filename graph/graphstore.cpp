//
// Created by squishy on 11/1/2016.
//

#include <vector>
#include "graphstore.h"
#include "graphproperties.h"
#include "graphics/colors.h"
#include <fstream>
#include <QtDebug>
#include <QString>
#include <functional>
#include <cmath>
/*
#ifdef __MINGW32__
// AAARRRRGH MINGW... WHY?
namespace std {
#include <string>
#include <sstream>

std::string to_string(const std::size_t i)
{
    std::ostringstream os;
    os << i;
    return os.str();
}
}
#endif
 */
#include "json.hpp"
using json = nlohmann::json;

namespace vcfsm {
namespace graph {
const double TAU = 6.283185307;
void GraphStore::dumpGraph(const std::string filename, const EditWindow *window)
{
    std::map<const graphics::Node*, int> nodeids;
    int count = 0;
    json nodes;
    json edges;
    json graph;
    for (graphics::Node * np: GraphProperties::nodeSet)
    {
        nodeids.insert({np, count});
        count++;
    }
    for (graphics::Node * np: GraphProperties::nodeSet)
    {
        json node;
        node["id"] = nodeids[np];
        if (GraphProperties::name(np) != "")
            node["name"] = GraphProperties::name(np);
        if (GraphProperties::label(np) != "")
            node["label"] = GraphProperties::label(np);
        nodes.push_back(node);
    }
    for (graphics::Edge * ep: GraphProperties::edgeSet)
    {
        edges.push_back({nodeids[ep->sourceNode()], nodeids[ep->destNode()]});
    }
    graph["nodes"] = nodes;
    graph["edges"] = edges;

    std::ofstream fout(filename);
    fout << graph.dump(2);
    fout.close();
    //qDebug() << QString::fromStdString(graph.dump());

}

void GraphStore::loadGraph(const std::string filename, EditWindow *window)
{
    std::ifstream gfile(filename.c_str());
    json graph;
    gfile >> graph;
    gfile.close();
    //qDebug() << QString::fromStdString(graph.dump());
    std::map <nlohmann::basic_json<>::value_type, pt2d> nodes;
    std::map <nlohmann::basic_json<>::value_type, graphics::Node *> graphNodes;
    double angle = TAU / graph["nodes"].size();
    double rad = 100.0;
    auto xpos = [angle, rad](int n) {return rad * std::cos(angle*n);};
    auto ypos = [angle, rad](int n) {return rad * std::sin(angle*n);};
    for (int i=0; i<graph["nodes"].size(); i++)
        nodes[graph["nodes"][i]["id"]] = {xpos(i), ypos(i)};
    //layout(nodes, graph["edges"]);
    window->clearGraph();
    for (int i=0; i<graph["nodes"].size(); i++)
    {
        graphics::Node * n = window->gw()->addNode();
        n->setPos({nodes[graph["nodes"][i]["id"]].x, nodes[graph["nodes"][i]["id"]].y});
        graphNodes[graph["nodes"][i]["id"]] = n;
        graph::GraphProperties::nodeSet.insert(n);
        if (graph["nodes"][i].find("name")!=graph["nodes"][i].end())
            graph::GraphProperties::setName(n, graph["nodes"][i]["name"]);
        if (graph["nodes"][i].find("label")!=graph["nodes"][i].end())
        {
            std::string label = graph["nodes"][i]["label"];
            graph::GraphProperties::setLabel(n, label);
            window->npp()->addLabel(label);
        }
    }

    qDebug() << "Number of edges:" << graph["edges"].size();
    for (int i=0; i<graph["edges"].size(); i++)
    {
        qDebug() << "From:" << graphNodes[graph["edges"][i][0]] << "To:" << graphNodes[graph["edges"][i][1]];
        graphics::Edge * e = new graphics::Edge(graphNodes[graph["edges"][i][0]], graphNodes[graph["edges"][i][1]]);
        graph::GraphProperties::edgeSet.insert(e);
        window->gw()->scene()->addItem(e);
        e->adjust();
    }
    window->gw()->forceLayout();
}

}
}
