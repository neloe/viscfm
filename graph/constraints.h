#ifndef CONSTRAINTS_H
#define CONSTRAINTS_H

#include "nautygraph.h"
#include <functional>
namespace vcfsm {
namespace graph {

typedef std::function<bool (const NautyGraph&)> constraint_t;
typedef std::function<constraint_t (const std::vector<std::string>&)> generator_t;

class Constraints
{
public:
    static constraint_t containsLabel(const std::vector<std::string> & args);
    static constraint_t containsName(const std::vector<std::string> & args);

    const static std::unordered_map<std::string, std::pair<generator_t, std::vector<std::string>>> generators;
};


}
}
#endif // CONSTRAINTS_H
