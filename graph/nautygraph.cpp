//
// Created by squishy on 11/10/16.
//

#include "nautygraph.h"
#include "graphproperties.h"
#include "nauty/traces.h"

#include <QtDebug>

namespace vcfsm {
namespace graph {

NautyGraph::NautyGraph()
{
    ids = std::make_shared<std::unordered_map<const graphics::Node *, int>>(
            std::unordered_map<const graphics::Node *, int>());
    revids = std::make_shared<std::vector<const graphics::Node *>>(std::vector<const graphics::Node *>());
    int count = 0;
    //labels = std::shared_ptr<int>(new int[GraphProperties::nodeSetStash.size()], std::default_delete<int[]>());
    //partition = std::shared_ptr<int>(new int[GraphProperties::nodeSetStash.size()], std::default_delete<int[]>());
    labels = std::make_shared<std::vector<int>>(std::vector<int>());
    partition = std::make_shared<std::vector<int>>(std::vector<int>());
    canonical_graph = nullptr;
    edges.resize(GraphProperties::nodeSetStash.size());
    std::unordered_map<std::string, std::unordered_set<int>> parts;

    revids->resize(GraphProperties::nodeSetStash.size());
    for (const graphics::Node *n: GraphProperties::nodeSetStash)
    {
        std::string lab = GraphProperties::label(n);
        ids->insert({n, count});
        (*revids)[count]=n;
        if (parts.find(lab) == parts.end())
            parts.insert({lab, std::unordered_set<int>()});
        parts.at(lab).insert(count);
        count++;
    }
    count = 0;
    for (auto it: parts)
    {
        int nodect = 1;
        for (int node: it.second)
        {
            labels->push_back(node);
            if (nodect != it.second.size())
                partition->push_back(1);
            else
                partition->push_back(0);
            nodect++;
            count++;
        }
    }

}


/*NautyGraph::NautyGraph(const NautyGraph &ng)
{
    labels = ng.labels;
    partition = ng.partition;
    revids = ng.revids;
    ids = ng.ids;
    connected_nodes = ng.connected_nodes;
    edges = ng.edges;
}*/

std::size_t NautyGraph::hash() const
{
    if (canonical_graph == nullptr)
        build_canonical();
    return std::hash<sparsegraph>()(*canonical_graph);
}

void NautyGraph::build_canonical() const
{
    // WHY ON EARTH DID I PUT THIS HERE? derp
    std::vector<int> evec;
    std::vector<std::size_t> vvec;
    std::vector<int> outDegree(edges.size());
    for (int i = 0; i < edges.size(); i++)
    {
        outDegree[i] = edges[i].size();
        vvec.push_back(evec.size());
        for (const int v: edges[i])
            evec.push_back(v);
    }
    sparsegraph g = {};// = {edges.size(), evec.size(), &outDegree[0],
    g.nv = edges.size();
    g.nde = evec.size();
    g.d = &(outDegree[0]);
    g.v = &(vvec[0]);
    g.e = &(evec[0]);
    g.dlen = outDegree.capacity();
    g.vlen = vvec.capacity();
    g.elen = evec.capacity();
    sparsegraph res = {};
    canonical_graph = std::make_shared<sparsegraph>(res);

    //DYNALLSTAT(int, orbits, orbits_sz);
    std::shared_ptr<int> orbits = std::shared_ptr<int>(new int[GraphProperties::nodeSetStash.size()], std::default_delete<int[]>());
    static DEFAULTOPTIONS_TRACES(options);
    options.defaultptn = false;
    options.getcanon = true;
    TracesStats stats;
    Traces(&g, &(labels->at(0)), &(partition->at(0)), orbits.get(), &options, &stats, canonical_graph.get());

}

void NautyGraph::addEdge(const graphics::Edge *e)
{
    int source, dest;
    gedges.insert(e);
    source = (*ids)[e->sourceNode()];
    dest = (*ids)[e->destNode()];
    connected_nodes.insert(source);
    connected_nodes.insert(dest);
    if (edges[source].find(dest)==edges[source].end())
    {
        qDebug() << "adding edge:" << source << dest;
        edges[source].insert(dest);
        edges[dest].insert(source);
    }
}

std::vector<NautyGraph> NautyGraph::augment() const
{
    std::vector<NautyGraph> results;
    std::unordered_set<graphics::Edge *> possible_edges;
    if (connected_nodes.size() == 0)
        possible_edges = GraphProperties::edgeSetStash;
    else
    {
        for (int n: connected_nodes)
        {
            for (graphics::Edge *e: (*revids)[n]->edges())
            {
                if (gedges.find(e)==gedges.end())
                {
                    possible_edges.insert(e);
                }
            }
        }
    }
    for (graphics::Edge *e: possible_edges)
    {
        NautyGraph newng (*this);
        newng.canonical_graph = nullptr;
        newng.addEdge(e);
        results.push_back(newng);
    }
    return results;

}

bool NautyGraph::operator==(const NautyGraph &rhs) const
{
    if (revids != rhs.revids)
        return false;
    for (int i = 0; i < edges.size(); i++)
    {
        if (!symDiff(edges[i], rhs.edges[i]).empty())
            return false;
    }
    return true;
}

std::unordered_set<int> NautyGraph::symDiff(const std::unordered_set<int> &a, const std::unordered_set<int> &b) const
{
    std::unordered_set<int> sdiff(a);

    for (int n: b)
    {
        if (sdiff.find(n) != sdiff.end())
            sdiff.erase(n);
        else
            sdiff.insert(n);
    }
    return sdiff;
}

const sparsegraph& NautyGraph::canonical() const
{
    if (canonical_graph == nullptr)
        build_canonical();
    return *canonical_graph;
}

const int NautyGraph::size() const
{
    int sum = 0;
    for (auto e: edges)
        sum += e.size();
    return sum / 2;
}

const std::string NautyGraph::labelOf(const int node) const
{
    return GraphProperties::label(revids->at(node));
}

const std::string NautyGraph::nameOf(const int node) const
{
    return GraphProperties::name(revids->at(node));
}

}
}
