//
// Created by squishy on 11/10/16.
//

#ifndef VCFSM_NAUTYGRAPH_H
#define VCFSM_NAUTYGRAPH_H

#include <memory>
#include <unordered_map>
#include <graphics/node.h>
#include <graphics/edge.h>
#include <nauty/nausparse.h>
#include <unordered_set>
#include <functional>
#include <vector>

namespace vcfsm {
namespace graph {



class NautyGraph {
    public:
        NautyGraph();
        //NautyGraph(const NautyGraph& ng);
        void build_canonical() const;
        std::size_t hash() const;
        void addEdge(const graphics::Edge * e);
        std::vector<NautyGraph> augment() const;

        const sparsegraph& canonical() const;
        bool operator==(const NautyGraph & rhs) const;

        const graphics::Node * nodeptr(const int n) const {return revids->at(n);}
        const std::unordered_set<int> & edgeDests(const int i) const {return edges[i];}
        const int size() const;
        const int nodes() const {return labels->size();}
        const std::unordered_set<int> & connNodes() const {return connected_nodes;}
        const std::string labelOf(const int node) const;
        const std::string nameOf(const int node) const;
    private:
        std::shared_ptr<std::vector<int>> labels;
        std::shared_ptr<std::vector<int>> partition;
        std::shared_ptr<std::vector<const graphics::Node*>> revids;
        std::shared_ptr<std::unordered_map<const graphics::Node*, int>> ids;
        mutable std::shared_ptr<sparsegraph> canonical_graph;
        std::unordered_set<int> connected_nodes;
        std::unordered_set<const graphics::Edge *> gedges;
        std::vector<std::unordered_set<int>> edges;

        std::unordered_set<int> symDiff(const std::unordered_set<int> & a, const std::unordered_set<int> & b) const;

};

struct GraphHashCmp
{
    bool operator() (const NautyGraph & lhs, const NautyGraph & rhs)
    {
        return lhs.hash() == rhs.hash();
    }
    bool operator() (const NautyGraph & lhs, const NautyGraph & rhs) const
    {
        return lhs.hash() == rhs.hash();
    }
};

}
}

namespace std {
inline void hash_combine(size_t & seed, size_t v)
{
    static std::hash<int> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}
template <> struct hash<sparsegraph>
{
    std::size_t operator() (sparsegraph const & sg) const
    {
        size_t h = std::hash<size_t>()(sg.nde);
        hash_combine(h, sg.nv);
        for (int i=0; i<sg.nv; i++)
        {
            hash_combine(h, sg.v[i]);
            hash_combine(h, sg.d[i]);
        }
        // this is probably wrong; what if the canonical representation
        // doesn't put the directed edges in the same order?
        for (int i=0; i<sg.nde; i++)
            hash_combine(h, sg.e[i]);
        return h;
    }
};
template <> struct hash<vcfsm::graph::NautyGraph>
{
    std::size_t operator() (vcfsm::graph::NautyGraph const& ng) const
    {
        return ng.hash();
    }
};
}

#endif //VCFSM_NAUTYGRAPH_H
