#include "constraints.h"

namespace vcfsm {
namespace graph {

constraint_t Constraints::containsLabel(const std::vector<std::string> & args)
{
    std::string label = args[0];
    return [label](const NautyGraph & ng) {
        for (int n: ng.connNodes())
        {
            if (ng.labelOf(n) == label)
                return true;
        }
        return false;
    };
}

constraint_t Constraints::containsName(const std::vector<std::string> & args)
{
    std::string label = args[0];
    return [label](const NautyGraph & ng) {
        for (int n: ng.connNodes())
        {
            if (ng.nameOf(n) == label)
                return true;
        }
        return false;
    };
}

const std::unordered_map<std::string, std::pair<generator_t, std::vector<std::string>>> Constraints::generators = {
        {"Contains label", {containsLabel, {"Label"}}},
        {"Contains name", {containsLabel, {"Name"}}}
    };

}
}
