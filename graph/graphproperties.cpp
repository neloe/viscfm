//
// Created by squishy on 10/13/2016.
//

#include "graphproperties.h"

namespace vcfsm {
namespace graph {

std::unordered_map<const graphics::Node*, std::string> GraphProperties::m_names = std::unordered_map<const graphics::Node*, std::string>();
std::unordered_map<const graphics::Node*, std::string> GraphProperties::m_labels = std::unordered_map<const graphics::Node*, std::string>();

std::unordered_set<graphics::Edge*> GraphProperties::edgeSet = std::unordered_set<graphics::Edge*>();
std::unordered_set<graphics::Edge*> GraphProperties::edgeSetStash = std::unordered_set<graphics::Edge*>();
std::unordered_set<graphics::Node*> GraphProperties::nodeSet = std::unordered_set<graphics::Node*>();
std::unordered_set<graphics::Node*> GraphProperties::nodeSetStash = std::unordered_set<graphics::Node*>();

std::string GraphProperties::defaultGet(std::unordered_map<const graphics::Node *, std::string> &map,
                                                const graphics::Node *key)
{
    auto it = map.find(key);
    if (it == map.end())
        return "";
    return it->second;
}
std::string GraphProperties::name(const graphics::Node * node)
{
    return defaultGet(m_names, node);
}

std::string GraphProperties::label(const graphics::Node *node)
{
    return defaultGet(m_labels, node);
}

void GraphProperties::stash()
{
    edgeSetStash = std::move(edgeSet);
    nodeSetStash = std::move(nodeSet);
    edgeSet = std::unordered_set<graphics::Edge *>();
    nodeSet = std::unordered_set<graphics::Node *>();
}

void GraphProperties::unstash()
{
    edgeSet = std::move(edgeSetStash);
    nodeSet = std::move(nodeSetStash);
    edgeSetStash = std::unordered_set<graphics::Edge *>();
    nodeSetStash = std::unordered_set<graphics::Node *>();
}

}
}
