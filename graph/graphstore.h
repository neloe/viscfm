//
// Created by squishy on 11/1/2016.
//

#ifndef VCFSM_GRAPHSTORE_H
#define VCFSM_GRAPHSTORE_H
#include <string>
#include <editwindow.h>

namespace vcfsm {
namespace graph {

struct pt2d {double x; double y;};

class GraphStore
{
    public:
        static void dumpGraph(const std::string filename, const EditWindow *window);
        static void loadGraph(const std::string filename, EditWindow *window);
};

}
}

#endif //VCFSM_GRAPHSTORE_H
