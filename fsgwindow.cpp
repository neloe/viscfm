//
// Created by squishy on 11/16/16.
//

#include <QDockWidget>
#include "fsgwindow.h"
#include "graph/graphproperties.h"
#include <QtDebug>
#include <cstdlib>

FsgWindow::FsgWindow(QWidget *parent): GraphWindow(parent)
{
    callback = (EditWindow *) parent;
    graphWidget = new vcfsm::graphics::GraphWidget(this);
    nodePropertyPanel = new vcfsm::graphics::NodePropPanel(this);
    editGraphPanel = nullptr;
    nodePropertyPanel->disableEditing();
    QDockWidget * nodePropertyDock = new QDockWidget(tr("Node Properties"), this);
    nodePropertyDock->setWidget(nodePropertyPanel);

    addDockWidget(Qt::RightDockWidgetArea, nodePropertyDock);
    QDockWidget * fsgDock = new QDockWidget(tr("FSG Control"));
    constraintPanel = new vcfsm::graphics::ConstraintPanel(this);
    fsgDock->setWidget(constraintPanel);
    addDockWidget(Qt::RightDockWidgetArea, fsgDock);

    setCentralWidget(graphWidget);
}

void FsgWindow::setLabelMaps(const std::unordered_map<std::string, QColor> &labels)
{
    graphWidget->label_mapping = labels;
    for (auto l: labels)
    {
        nodePropertyPanel->addToCombo(l.first);
    }
}

void FsgWindow::drawGraphs(const std::vector<vcfsm::graph::NautyGraph> &graphs)
{
    for (const vcfsm::graph::NautyGraph & ng: graphs)
    {
        drawGraph(ng);
    }
    graphWidget->forceLayout();
}

vcfsm::graphics::Node * FsgWindow::getMapping(std::unordered_map<const vcfsm::graphics::Node *,
        vcfsm::graphics::Node *> & mapping, const vcfsm::graphics::Node *n)
{
    if (mapping.find(n) == mapping.end())
    {
        qDebug() << "creating new for" << n;
        mapping.insert({n, graphWidget->addNode()});
    }
    return mapping.at(n);
}
void FsgWindow::drawGraph(const vcfsm::graph::NautyGraph &ng)
{
    static int count = 0;
    qDebug() << "Drawing graph" << count++;
    vcfsm::graphics::Node * source, * dest;
    std::unordered_map<const vcfsm::graphics::Node *, vcfsm::graphics::Node *> nodemapping;
    for (int node = 0; node < ng.nodes(); node++)
    {
        if (ng.edgeDests(node).size() > 0)
        {
            //double xpos = qrand()%100 * .001, ypos = qrand()%100 * .001;
            vcfsm::graphics::Node * source = getMapping(nodemapping, ng.nodeptr(node));
            //source->setPos(xpos, ypos);
            vcfsm::graph::GraphProperties::setName(source, vcfsm::graph::GraphProperties::name(ng.nodeptr(node)));
            vcfsm::graph::GraphProperties::setLabel(source, vcfsm::graph::GraphProperties::label(ng.nodeptr(node)));
            //qDebug() << QString::fromStdString(vcfsm::graph::GraphProperties::name(ng.nodeptr(node))) << QString::fromStdString(vcfsm::graph::GraphProperties::label(ng.nodeptr(node)));
            for (int destidx: ng.edgeDests(node))
            {
                dest = getMapping(nodemapping, ng.nodeptr(destidx));
                vcfsm::graphics::Edge * e =new vcfsm::graphics::Edge(source, dest);
                graphWidget->scene()->addItem(e);
                e->adjust();
                qDebug() << ng.nodeptr(node) << source << ng.nodeptr(destidx) << dest;
            }
        }
    }
}

void FsgWindow::closeEvent(QCloseEvent * event)
{
    vcfsm::graph::GraphProperties::unstash();
    callback->show();
    QMainWindow::closeEvent(event);
}
