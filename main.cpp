#include <iostream>
#include "graphics/graphwidget.h"
#include "graphics/editpanel.h"
#include <QApplication>
#include <QTime>
#include <QMainWindow>
#include <QDockWidget>
#include <graphics/nodeproppanel.h>
#include "editwindow.h"
using namespace vcfsm;

int main(int argc, char* argv[])
{
    QApplication app( argc, argv);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
/*
    graphics::GraphWidget * widget = new graphics::GraphWidget;
    graphics::EditPanel * edit = new graphics::EditPanel;
    graphics::NodePropPanel * nodeProp = new graphics::NodePropPanel;
    edit->setGraphWidget(widget);
    widget->propPanel = nodeProp;
    nodeProp->m_gw = widget;
    QMainWindow mainWindow;
    QDockWidget * newNodeDock = new QDockWidget(QMainWindow::tr("Graph Editor"), &mainWindow);
    QDockWidget * nodePropDock = new QDockWidget(QMainWindow::tr("Node Properties"), &mainWindow);
    newNodeDock->setAllowedAreas(Qt::RightDockWidgetArea);
    mainWindow.setCentralWidget(widget);
    newNodeDock->setWidget(edit);
    mainWindow.addDockWidget(Qt::RightDockWidgetArea, newNodeDock);

    nodePropDock->setAllowedAreas((Qt::RightDockWidgetArea));
    nodePropDock->setWidget(nodeProp);
    mainWindow.addDockWidget(Qt::RightDockWidgetArea, nodePropDock);
*/
    EditWindow mainWindow;



    mainWindow.show();
    return app.exec();
}