#include "constraintpanel.h"
#include <QListWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QHBoxLayout>
#include <graphics/constraintbuilder.h>
#include <QtDebug>
#include <graph/freqsubgraphs.h>
#include "fsgwindow.h"
#include "graph/graphproperties.h"
namespace vcfsm {
namespace graphics {

ConstraintPanel::ConstraintPanel(QWidget * parent): QWidget(parent)
{
    QVBoxLayout * layout = new QVBoxLayout();
    par = (FsgWindow *) parent;

    layout->addWidget(new QLabel("Constraints"));
    m_constraints = new QListWidget(this);
    layout->addWidget(m_constraints);
    QPushButton * addConstraintBtn = new QPushButton(this);
    addConstraintBtn->setIcon(QIcon(QPixmap(":/graphics/icons/add.png")));
    addConstraintBtn->setIconSize(QSize(16,16));
    layout->addWidget(addConstraintBtn);

    QWidget * supportGroup = new QWidget(this);
    QHBoxLayout * support = new QHBoxLayout(this);
    support->addWidget(new QLabel("Min Support:"));
    m_minsupport = new QSpinBox(this);
    m_minsupport->setMinimum(1);
    support->addWidget(m_minsupport);
    supportGroup->setLayout(support);
    layout->addWidget(supportGroup);

    QWidget * filterGroup = new QWidget(this);
    QHBoxLayout * filter = new QHBoxLayout(this);
    QPushButton * filterBtn = new QPushButton(this);
    QPushButton * augmentBtn = new QPushButton(this);
    filterBtn->setText("Constrain FSGs");
    augmentBtn->setText("Edges++");
    filter->addWidget(filterBtn);
    filter->addWidget(augmentBtn);
    filterGroup->setLayout(filter);
    layout->addWidget(filterGroup);

    setLayout(layout);

    connect(addConstraintBtn, &QPushButton::clicked, this, &ConstraintPanel::addConstraint);
    connect(filterBtn, &QPushButton::clicked, this, &ConstraintPanel::filter);
    connect(augmentBtn, &QPushButton::clicked, this, &ConstraintPanel::augment);
}

void ConstraintPanel::addConstraint(bool clicked)
{
    QListWidgetItem * newitem = ConstraintBuilder::getConstraint(this);
    if (newitem != nullptr)
    {
        qDebug() << "Adding value to list";
        m_constraints->addItem(newitem);
        qDebug() << "List has" << m_constraints->count();
    }
}

void ConstraintPanel::filter(bool clicked)
{
    std::vector<graph::constraint_t> constraints;
    //for (QListWidgetItem * item : m_constraints->items())
    for (int i=0; i<m_constraints->count(); i++)
    {
        QListWidgetItem * item = m_constraints->item(i);
        constraints.push_back(item->data(Qt::UserRole).value<graph::constraint_t>());
    }
    int minsupport = m_minsupport->value();
    par->gw()->clearCanvas();
    qDebug() << "----" <<  par->gw()->label_mapping.size();
    vcfsm::graph::GraphProperties::nodeSet.clear();
    vcfsm::graph::GraphProperties::edgeSet.clear();

    if (m_constraints->count() > 0)
        par->drawGraphs(par->fsgs->FSGs(constraints, minsupport));
    else
        par->drawGraphs(par->fsgs->FSGs(minsupport));

}

void ConstraintPanel::augment(bool clicked)
{
    par->fsgs->augmentSubgraphs();
    filter(clicked);
}

}
}
