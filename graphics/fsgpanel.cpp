//
// Created by squishy on 11/15/16.
//

#include "fsgpanel.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QMessageBox>
#include "graph/freqsubgraphs.h"
#include "fsgwindow.h"
#include <memory>
#include "graph/graphproperties.h"
#include <QtDebug>
namespace vcfsm {
namespace graphics {

FsgPanel::FsgPanel(QWidget * parent): QWidget(parent)
{
    ew = (EditWindow *) parent;
    QPushButton * fsgButton = new QPushButton(this);
    fsgButton->setText("FSG");
    QHBoxLayout * layout = new QHBoxLayout();
    layout->addWidget(fsgButton);
    setLayout(layout);
    connect(fsgButton, &QPushButton::clicked, this, &FsgPanel::fsg);
}

void FsgPanel::fsg(bool clicked)
{
    //std::shared_ptr<graph::FreqSubGraphs> fsgs (new graph::FreqSubGraphs());
    graph::GraphProperties::stash();
    std::shared_ptr<graph::FreqSubGraphs> fsgs(new graph::FreqSubGraphs);
    //fsgs.augmentSubgraphs();
    //QMessageBox::warning(this, "fsg of size 2",
    //                     QString::fromStdString(std::to_string(fsgs.FSGs().size())));
    FsgWindow * fsgw = new FsgWindow(ew);
    fsgw->setLabelMaps(ew->gw()->label_mapping);
    fsgw->fsgs = fsgs;
    fsgw->drawGraphs(fsgs->FSGs(1));
    ew->hide();
    fsgw->show();
}
}
}
