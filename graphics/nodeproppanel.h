//
// Created by squishy on 10/13/2016.
//

#ifndef VCFSM_NODEPROPPANEL_H
#define VCFSM_NODEPROPPANEL_H

#include <QWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>

class GraphWindow;

namespace vcfsm{
namespace graphics {

class Node;
class GraphWidget;

class NodePropPanel: public QWidget
{
    public:
        NodePropPanel(QWidget * parent=Q_NULLPTR);
        void load(const Node* node);
        void addLabel(const std::string label);
        void addToCombo(const std::string label) {m_nodeLabelField->addItem(QString::fromStdString(label));}
        void disableEditing() {m_nodeLabelField->setDisabled(true); m_nodeNameField->setDisabled(true); addLabelBtn->setDisabled(true);}


    private:
        QLineEdit * m_nodeNameField;
        QComboBox * m_nodeLabelField;
        QPushButton * addLabelBtn;
        GraphWindow * m_parent;
        GraphWindow *par(){return m_parent;}
        
    private slots:
        void setNodeName(const QString & str);
        void setNodeLabel(const QString & str);
        void makeNewLabel();

};

}
}

#endif //VCFSM_NODEPROPPANEL_H
