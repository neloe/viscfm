#ifndef CONSTRAINTARG_H
#define CONSTRAINTARG_H

#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QLineEdit>

namespace vcfsm {
namespace graphics {


class ConstraintArg: public QFrame
{
public:
    ConstraintArg(QWidget * parent=Q_NULLPTR);
    void setName(const std::string & name);
    void setText(const std::string & value);
    const std::string value() const;

private:
    QLabel * m_name;
    QLineEdit * m_value;
};

}
}
#endif // CONSTRAINTARG_H
