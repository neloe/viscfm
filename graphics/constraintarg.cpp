#include "constraintarg.h"
#include <QHBoxLayout>

namespace vcfsm {
namespace graphics {

ConstraintArg::ConstraintArg(QWidget * parent):QFrame(parent)
{
    m_name = new QLabel(this);
    m_value = new QLineEdit(this);
    QHBoxLayout * layout = new QHBoxLayout(this);
    layout->addWidget(m_name);
    layout->addWidget(m_value);
    setLayout(layout);
}

void ConstraintArg::setName(const std::string & name)
{
    m_name->setText(QString::fromStdString(name));
}

void ConstraintArg::setText(const std::string & value)
{
    m_value->setText(QString::fromStdString(value));
}

const std::string ConstraintArg::value() const
{
    return m_value->text().toStdString();
}

}
}
