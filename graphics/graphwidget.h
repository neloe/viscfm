//
// Created by squishy on 10/11/2016.
//

#ifndef VCFSM_GRAPHWIDGET_H
#define VCFSM_GRAPHWIDGET_H

#include <QGraphicsView>
#include <unordered_map>
#include <unordered_set>
#include "edge.h"

class GraphWindow;
namespace vcfsm {
namespace graphics {

class Node;
class Edge;
class NodePropPanel;
class GraphWidget: public QGraphicsView
{
        Q_OBJECT
    public:
        enum edgeModes {NONE, SINGLE, PATH, STAR};
        edgeModes currEdgeMode;
        GraphWidget(QWidget *parent = 0);

        void itemMoved();
        Node * addNode();
        void updateSelected (Node * sel);

        Node * selectedNode;
        std::unordered_map<std::string, QColor> label_mapping;
        void clear();
        void enableLayout() {m_layout = true;}
        void disableLayout() {m_layout = false;}
        void forceLayout();
        void clearCanvas() {m_scene->clear();}

    protected:
        void keyPressEvent(QKeyEvent * event) Q_DECL_OVERRIDE;
        void timerEvent(QTimerEvent * event) Q_DECL_OVERRIDE;
        void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
        //void drawBackground(QPainter * painter, const QRectF & rect) Q_DECL_OVERRIDE;
        void scaleView(qreal scaleFactor);
        GraphWindow * m_parent;
        GraphWindow * par() {return m_parent;}

    private:
        QGraphicsScene * m_scene;
        int timerID;
        bool m_layout;
//        std::unordered_set<Edge*> edgeSet;
//        std::unordered_set<Node*> nodeSet;

        friend class Node;

};

}
}

#endif //VCFSM_GRAPHWIDGET_H
