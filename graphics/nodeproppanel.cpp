//
// Created by squishy on 10/13/2016.
//

#include "nodeproppanel.h"
#include "graph/graphproperties.h"
#include "graphwidget.h"
#include "colors.h"
#include "editwindow.h"

#include <QFormLayout>
#include <QPushButton>
#include <QInputDialog>
#include <QtDebug>
namespace vcfsm{
namespace graphics {

NodePropPanel::NodePropPanel(QWidget *parent): QWidget(parent)
{
    m_nodeNameField = new QLineEdit;
    m_nodeLabelField = new QComboBox;
    m_nodeLabelField->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    m_parent = (GraphWindow *) parent;
    QFormLayout * layout = new QFormLayout(this);
    QHBoxLayout * labelLayout = new QHBoxLayout(this);
    addLabelBtn = new QPushButton ();
    addLabelBtn->setIcon(QIcon(QPixmap(":/graphics/icons/add.png")));
    addLabelBtn->setIconSize(QSize(16,16));
    labelLayout->addWidget(m_nodeLabelField);
    labelLayout->addWidget(addLabelBtn);
    layout->addRow("Name", m_nodeNameField);
    layout->addRow("Label", labelLayout);
    m_nodeLabelField->addItem("");

    setLayout(layout);

    connect(m_nodeNameField, &QLineEdit::textEdited, this, &NodePropPanel::setNodeName);
    connect(m_nodeLabelField, &QComboBox::currentTextChanged, this, &NodePropPanel::setNodeLabel);
    connect(addLabelBtn, &QPushButton::clicked, this, &NodePropPanel::makeNewLabel);
//    connect(m_nodeLabelField, &QLineEdit::textEdited, this, &GraphProperties::setLabel);
}

void NodePropPanel::load(const Node *node)
{
    // for now, just name
    m_nodeNameField->setText(QString::fromStdString(vcfsm::graph::GraphProperties::name(node)));
    m_nodeLabelField->setCurrentText(QString::fromStdString(vcfsm::graph::GraphProperties::label(node)));
}

void NodePropPanel::setNodeName(const QString &str)
{
    vcfsm::graph::GraphProperties::setName(par()->gw()->selectedNode, str.toStdString());
}

void NodePropPanel::setNodeLabel(const QString &str)
{
    vcfsm::graph::GraphProperties::setLabel(par()->gw()->selectedNode, str.toStdString());
    par()->gw()->selectedNode->update();
}

void NodePropPanel::makeNewLabel()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("New Label"),
                                         tr("Label"), QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty())
    {
        addLabel(text.toStdString());
        
        if (par()->gw()->selectedNode)
        {
            m_nodeLabelField->setCurrentText(text);
        }
    }
}

void NodePropPanel::addLabel(std::string label)
{
    QString text = QString::fromStdString(label);
    if (par()->gw()->label_mapping.find(label)==par()->gw()->label_mapping.end())
    {
        m_nodeLabelField->addItem(text);
        par()->gw()->label_mapping.insert({text.toStdString(), kelly[par()->gw()->label_mapping.size()+1]});
        //qDebug() << "make label, new size:" << par()->graphWidget->label_mapping.size();
    }
}
}
}
