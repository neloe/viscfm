//
// Created by squishy on 10/11/2016.
//

#include "edge.h"
#include <QPainter>

namespace vcfsm {
namespace graphics {

Edge::Edge(Node *source, Node *dest)
{
    setAcceptedMouseButtons(0);
    m_source = source;
    m_dest = dest;
    source -> addEdge(this);
    dest -> addEdge(this);
    adjust();
}

void Edge::adjust()
{
    if (!m_source || !m_dest)
        return;
    QLineF line(mapFromItem(m_source, 0, 0), mapFromItem(m_dest, 0, 0));
    qreal length = line.length();

    prepareGeometryChange();

    if (length > qreal(20.))
    {
        QPointF edgeOffset((line.dx() * 10) / length, (line.dy() * 10) / length);
        m_sourcePt = line.p1() + edgeOffset;
        m_destPt = line.p2() - edgeOffset;
    }
    else
        m_sourcePt = m_destPt = line.p1();

}

QRectF Edge::boundingRect() const
{
    if (!m_source || !m_dest)
        return QRectF();

    qreal penWidth = 1;
    qreal extra = penWidth * .5;
    return QRectF(m_sourcePt, QSizeF(m_destPt.x() - m_sourcePt.x(), m_destPt.y() - m_sourcePt.y())).normalized().adjusted(-extra, -extra, extra, extra);
}

void Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!m_source || !m_dest)
        return;
    QLineF line (m_sourcePt, m_destPt);
    if (qFuzzyCompare(line.length(), qreal(0.)))
        return;

    // line draw!
    painter->setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter->drawLine(line);
    // no arrows, so we can avoid the rest of it from the edge.cpp example
}

bool Edge::operator==(const Edge & e) const
{
    std::hash<Edge*> edgeHash;
    return edgeHash(&e) == edgeHash(this);
}

}
}