//
// Created by squishy on 10/11/2016.
//

#ifndef VCFSM_NODE_H
#define VCFSM_NODE_H

#include <QGraphicsItem>
#include <QList>

class QGraphicsSceneMouseEvent;

class EditWindow;

namespace vcfsm {
namespace graphics {

class Edge;
class GraphWidget;

class Node : public QGraphicsItem
{
    public:
        Node(GraphWidget * graphWidget);
        void addEdge(Edge * edge);
        QList <Edge *> edges() const {return m_edgeList;}

        enum {Type = UserType + 1};
        int type() const Q_DECL_OVERRIDE {return Type;}

        QRectF boundingRect() const Q_DECL_OVERRIDE;
        QPainterPath shape() const Q_DECL_OVERRIDE;
        void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget) Q_DECL_OVERRIDE;
        void calculateForces();
        bool advance();

    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
        void mousePressEvent(QGraphicsSceneMouseEvent * event) Q_DECL_OVERRIDE;
        void mouseReleaseEvent(QGraphicsSceneMouseEvent * event) Q_DECL_OVERRIDE;

    private:
        unsigned int m_id;
        QList<Edge *> m_edgeList;
        QPointF m_newPos;
        GraphWidget * m_graph;
};

}
}

#endif //VCFSM_NODE_H
