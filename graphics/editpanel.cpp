//
// Created by squishy on 10/13/2016.
//

#include "editpanel.h"
#include "graphwidget.h"
#include "editwindow.h"
#include <QHBoxLayout>
#include <QtDebug>

namespace vcfsm {
namespace graphics {

EditPanel::EditPanel(QWidget *parent) : QWidget(parent)
{
    m_parent = (EditWindow *)parent;
    QLayout * layout = new QVBoxLayout;
    QPushButton * addNode = new QPushButton("Add Node");
    addEdge = new QPushButton("Add Edge: Start/End");
    addPath = new QPushButton("Add Edges: Path");
    addStar = new QPushButton("Add Edges: Star");
    addEdge->setCheckable(true);
    addPath->setCheckable(true);
    addStar->setCheckable(true);
    layout->addWidget(addNode);
    layout -> addWidget(addEdge);
    layout -> addWidget(addPath);
    layout -> addWidget(addStar);
    setLayout(layout);

    connect(addNode, &QPushButton::clicked, this, &EditPanel::addNode);
    connect(addEdge, &QPushButton::toggled, this, &EditPanel::singleEdgeMode);
    connect(addStar, &QPushButton::toggled, this, &EditPanel::starEdgeMode);
    connect(addPath, &QPushButton::toggled, this, &EditPanel::pathEdgeMode);
}

void EditPanel::addNode()
{
    par()->graphWidget->updateSelected(par()->graphWidget->addNode());
}

void EditPanel::singleEdgeMode(bool checked)
{
    if (checked)
    {
        addPath->setChecked(false);
        addStar->setChecked(false);
        par()->graphWidget->updateSelected(NULL);
        par()->graphWidget->currEdgeMode = GraphWidget::SINGLE;
    }
    else
    {
        if (par()->graphWidget->currEdgeMode == GraphWidget::SINGLE)
            par()->graphWidget->currEdgeMode = GraphWidget::NONE;
    }
}

void EditPanel::pathEdgeMode(bool checked)
{
    if (checked)
    {
        addEdge->setChecked(false);
        addStar->setChecked(false);
        par()->graphWidget->updateSelected(NULL);
        par()->graphWidget->currEdgeMode = GraphWidget::PATH;
    }
    else
    {
        if (par()->graphWidget->currEdgeMode == GraphWidget::PATH)
            par()->graphWidget->currEdgeMode = GraphWidget::NONE;
    }
}

void EditPanel::starEdgeMode(bool checked)
{
    if (checked)
    {
        addEdge->setChecked(false);
        addPath->setChecked(false);
        par()->graphWidget->updateSelected(NULL);
        par()->graphWidget->currEdgeMode = GraphWidget::STAR;
    }
    else
    {
        if (par()->graphWidget->currEdgeMode == GraphWidget::STAR)
            par()->graphWidget->currEdgeMode = GraphWidget::NONE;
    }
}

}
}