//
// Created by squishy on 11/15/16.
//

#ifndef VCFSM_FSGPANEL_H
#define VCFSM_FSGPANEL_H
#include <QWidget>
#include "editwindow.h"
namespace vcfsm {
namespace graphics {

class FsgPanel: public QWidget
{
    public:
        FsgPanel(QWidget * parent);

    private:
        EditWindow * ew;

    public slots:
        void fsg(bool clicked);
};

}
}

#endif //VCFSM_FSGPANEL_H
