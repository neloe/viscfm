#include "constraintbuilder.h"
#include <QComboBox>
#include <graph/constraints.h>
#include <QPushButton>
#include <QtDebug>
#include <QMessageBox>
#include <QVariant>
namespace vcfsm {
namespace graphics {

QListWidgetItem * ConstraintBuilder::item = nullptr;

ConstraintBuilder::ConstraintBuilder(QWidget * parent): QDialog(parent)
{
    setModal(false);
    setWindowTitle("Add New Constraint");
    m_layout = new QVBoxLayout(this);
    argsLayout = new QVBoxLayout(this);
    QHBoxLayout * buttonPanel = new QHBoxLayout(this);
    buttonPanel->setAlignment(Qt::AlignRight);
    QPushButton * okBtn = new QPushButton(this);
    QPushButton * cancelBtn = new QPushButton(this);
    okBtn->setText("OK");
    cancelBtn->setText("Cancel");
    buttonPanel->addWidget(okBtn);
    buttonPanel->addWidget(cancelBtn);
    constraint_type = new QComboBox(this);
    for (auto constraint: graph::Constraints::generators)
        constraint_type->addItem(QString::fromStdString(constraint.first));
    m_layout->addWidget(constraint_type);
    m_layout->addLayout(argsLayout);
    m_layout->addLayout(buttonPanel);
    setLayout(m_layout);

    auto selected = graph::Constraints::generators.find(constraint_type->currentText().toStdString());
    if (selected != graph::Constraints::generators.end())
    {
        for (int i=0; i < selected->second.second.size(); i++)
        {
            ConstraintArg * arg = new ConstraintArg(this);
            arg->setName(selected->second.second[i]);
            args.push_back(arg);
            argsLayout->addWidget(arg);
        }
    }

    connect(constraint_type, &QComboBox::currentTextChanged, this, &switchConstraint);
    connect(okBtn, &QPushButton::clicked, this, &ConstraintBuilder::ok);
    connect (cancelBtn, &QPushButton::clicked, this, &ConstraintBuilder::cancel);
}

QListWidgetItem * ConstraintBuilder::getConstraint(QWidget * parent)
{
    ConstraintBuilder cb(parent);
    cb.exec();
    return item;
}

void ConstraintBuilder::switchConstraint(const QString & sel)
{
    auto newargs = graph::Constraints::generators.find(sel.toStdString())->second.second;
    int diff = newargs.size() - args.size();
    for (int i=0; i<diff; i++)
    {
        ConstraintArg * arg = new ConstraintArg(this);
        args.push_back(arg);
        argsLayout->addWidget(arg);
    }
    for (int i=0; i<args.size(); i++)
    {
        if (i < newargs.size())
        {
            args[i]->setName(newargs[i]);
            args[i]->show();
            args[i]->setText("");
        }
        else
        {
            args[i]->hide();
        }
    }
}

void ConstraintBuilder::ok(const bool triggered)
{
    auto selected = graph::Constraints::generators.find(constraint_type->currentText().toStdString());
    int reqargs = selected->second.second.size();
    std::vector<std::string> constraint_args;
    QString arglist = "";
    for (int i=0; i<reqargs; i++)
    {
        if (args[i]->value() == "")
        {
            QMessageBox::information(this, "Too Few Arguments", "Please fill all argument fields.");
            return;
        }
        arglist += QString::fromStdString(args[i]->value());
        constraint_args.push_back(args[i]->value());
        if (i < reqargs - 1)
            arglist += ", ";
    }
    item = new QListWidgetItem();
    QString itemText = QString::fromStdString(selected->first) + ": " + arglist;
    item->setText(itemText);
    QVariant data;
    data.setValue(selected->second.first(constraint_args));
    item->setData(Qt::UserRole, data);
    close();
}

void ConstraintBuilder::cancel(const bool triggered)
{
    item = nullptr;
    close();
}

}
}
