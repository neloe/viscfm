# Visual Constraint Based Frequent Subgraph Mining

This project serves as a C++/Qt based implementation of the Visual Constraint Based Frequent Subgraph Mining research presented at DMS '16.  

When time allows, it will be licensed under the (L)GPLv3 to conform with the Open Source version of QT.

Until that time, the licence is implicitly the (L)GPLv3.

## Development requirements
* Qt 5 (https://www.qt.io/download-open-source/)
* CMake
* MinGW (for development on Windows and CLion, not QT Creator)

## Qt Creator
At this time, it is recommended to use QT Creator for building the project.
Make sure you have CMake and Qt 5 installed, and tell QT Creator which CMake executable it should use.
Then, you should be able to open the CMakeLists.txt file in QT Creator and build the project.

## Setting up Nauty
You will need to run ./configure from the nauty directory in the project before it will build and run; this is so the necessary files will be automatically generated for the platform you are building on.

## Configuring CLion for Windows
CLion is the JetBrains C/C++ IDE and has a free educational version for research and students, which is available here: https://www.jetbrains.com/clion/.

To easily develop this project with CLion, install MinGW (if on Windows), and add it to your path (I added `C:\MinGW\bin` to my PATH).  Additionally, after installing Qt, add the bin directory to your path (I added `C:\Qt\5.7\mingw53_32\bin` to my PATH).  

Once you install CLion and configure it to look for your instalation of MinGW, you can easily run the vcfsm executable from the IDE by selecting it from the run menu.

## NOTE:
Much of the code in the graphics/ directory was pulled directly or indirectly from the elastic-nodes example for QT5.  The code is still being modified to suit our needs, but the inspiration was from that.
